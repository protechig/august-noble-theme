<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package august noble
 */

get_header(); ?>

<div class="primary content-area">
<main id="main" class="site-main">
	
<div class="grid-x">
	<h1 class="page-title center-text">ARTICLES</h1>
<div class="articles-flex">
<?php
if ( have_posts() ) :
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
							* Include the Post-Format-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/
						get_template_part( 'template-parts/content', 'blog' );

					endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
?>
</div>
</div>
</main><!-- #main -->
</div><!-- .primary -->


<section class="grid-container  newsletter-signup" style="background-color:<?php the_field( 'background_color', 'option' ); ?>">
	<div class="grid-x">
	<h2 class="subscribe-header"><?php the_field( 'header_text', 'option' ); ?></h2>
	<h3><?php the_field( 'subtitle_text', 'option' ); ?></h3>
	<?php
		$form = get_field( 'subscribe_form', 'option' );
		gravity_form( $form, false, true, false, '', true, 1 );
	?>
	</div><!-- .grid-x -->
</section><!-- .cta-block -->
<?php get_footer(); ?>

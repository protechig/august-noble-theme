<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package august noble
 */

?>


<article <?php post_class(); ?>>
<div class="post-content">
<?php
if ( has_post_thumbnail() ) {
?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php the_post_thumbnail( 'blog-thumb' ); ?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php } ?>

	<div class="entry-content">
	<div class="inner-content">

	<header class="entry-header">
		<?php
			the_title( '<h3 class="center-text entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		?>
	</header><!-- .entry-header -->
	<div class="exerpet">
		<?php

			echo esc_html( august_custom_exerpt( 20 ) );
		?>
	</div>
	</div>
	</div><!-- .entry-content -->
</div>

<div class="read-more">
		<a class="read-more-btn" href="<?php echo esc_url( get_permalink() ); ?>">Continue reading</a>
</div>
</article><!-- #post-## -->


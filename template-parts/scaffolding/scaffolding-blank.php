<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package august noble
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'augustnoble' ); ?></h2>
</section>

<?php
/**
 *  The template used for displaying fifty/fifty media/text.
 *
 * @package august noble
 */

// Set up fields.
$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container testimonials', // The class of the container.
	 )
	);
?>
<div class="cleints-testimony grid-x<?php echo esc_attr( $animation_class ); ?>">

<?php

if ( have_rows( 'clients_testimonials' ) ) :

// Loop through the rows of data.
while ( have_rows( 'clients_testimonials' ) ) :
		the_row();


		?>
<div class="testimonial client-review-<?php echo ( get_row_index() % 2 == 0 ) ? 'even' : 'odd'; ?>">
<h2 class="name"><?php the_sub_field( 'clinent_name' ); ?></h2>
<h4 class=" business-info"><?php the_sub_field( 'company_position' ); ?><a href="<?php echo esc_url( $company_url ); ?>"><?php the_sub_field( 'company_name' ); ?></a></h4>
<div class="review">
<div class="client-img">
	<?php
	$image = get_sub_field( 'client_image' );
	$size  = 'client'; // (thumbnail, medium, large, full or custom size)
?>
<a class="covering-image" href="<?php the_sub_field( 'client_site_url' ); ?>"><?php echo wp_get_attachment_image( $image, $size ); ?></a>

</div>
<div class="testimony">
	<?php the_sub_field( 'testimonial' ); ?>
</div>
</div>
</div>

	<?php
endwhile;
endif;
?>

	</div><!-- .grid-x -->
</section><!-- .fifty-media-text -->

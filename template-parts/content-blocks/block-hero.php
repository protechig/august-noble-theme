<?php
/**
 * The template used for displaying a Hero block.
 *
 * @package august noble
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$text            = get_sub_field( 'text' );
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button_text' );
$animation_class = august_noble_get_animation_class();

// Start a <container> with possible block options.
august_noble_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container hero', // Container class.
	)
);
?>
<div class="grid-x">
	<div class="hero-content<?php echo esc_attr( $animation_class ); ?>">
		<?php if ( $title ) : ?>
			<h1 class="hero-title"><?php echo esc_html( $title ); ?></h1>
		<?php endif; ?>

		<?php if ( $text ) : ?>
			<h4 class="hero-description"><?php echo esc_html( $text ); ?></h4>
		<?php endif; ?>

		<?php if ( $button_text && $button_url ) : ?>
			<a class="button button-hero blue" href="<?php echo esc_url( $button_url ); ?>"><?php echo esc_html( $button_text ); ?></a>
		<?php endif; ?>
	</div><!-- .hero-content-->
	</div>
</section><!-- .hero -->

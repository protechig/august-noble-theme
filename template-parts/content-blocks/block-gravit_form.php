<?php
/**
 * The template used for displaying a CTA block.
 *
 * @package august noble
 */

// Set up fields.
$animation_class = august_noble_get_animation_class();

// Start a <container> with possible block options.
august_noble_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'grid-container gravit-form', // Container class.
	)
);
?>
	<div class="grid-x<?php echo esc_attr( $animation_class ); ?>">
<div class="the-form">
	<?php
		$form = get_sub_field( 'contact_form' );
		gravity_form( $form, false, true, false, '', true, 1 );
	?>
 </div>
	</div><!-- .grid-x -->
</section><!-- .cta-block -->

<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package august noble
 */

// Set up fields.
$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'grid-container logos', // Container class.
	 )
	);
?>

<div class="grid-x<?php echo esc_attr( $animation_class ); ?>">
<h2 class="center-text"><?php the_sub_field( 'section_header' ); ?></h2>	
<div class="company-logo">
<?php

if ( have_rows( 'client_logos' ) ) :

// Loop through the rows of data.
while ( have_rows( 'client_logos' ) ) :
		the_row();

		// Display a sub field value.
		$image = get_sub_field( 'image' );
		$size  = 'logo'; // (thumbnail, medium, large, full or custom size)

			?>
			<a class="covering-image" href="<?php the_sub_field( 'client_site_url' ); ?>"><?php echo wp_get_attachment_image( $image, $size ); ?></a>
			<?php



endwhile;
endif;
?>
</div>	
</div><!-- .grid-x -->
</section><!-- .fifty-text-media -->

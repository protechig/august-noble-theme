<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package august noble
 */

// Set up fields.
$header      = get_sub_field( 'header' );
$subtitle    = get_sub_field( 'subtitle_header' );
$tagline     = get_sub_field( 'page_tagline' );
$image_data  = get_sub_field( 'profile_image' );
$button_text = get_sub_field( 'contact_button' );
$button_link = get_sub_field( 'contact_link' );
$summary     = get_sub_field( 'summary' );

$animation_class = august_noble_get_animation_class();

// Start a <container> with a possible media background.
august_noble_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => ' content-block grid-container  august-services', // Container class.
	 )
	);
?>
	<div class=" profile grid-x<?php echo esc_attr( $animation_class ); ?>">
	<div class=" services">
		<div class=" page-header-inner">
				<h2> <?php echo esc_html( $header ); ?></h2>
				<h3><?php echo force_balance_tags( $tagline ); // WPCS: XSS OK. ?></h3>
			<ul>
			<?php
			if ( have_rows( 'services_list' ) ) :
				while ( have_rows( 'services_list' ) ) :
				the_row();
					?>
					<li><?php the_sub_field( 'services' ); ?></li>
					<?php
							endwhile;
						endif;

			?>
			</ul>
			
		</div>
		
	</div>
	<div class="covering-image">
	<?php
	$size = 'august'; // ( custom size)
	echo wp_get_attachment_image( $image_data, $size );
		?>
	</div>

	</div><!-- .grid-x -->
</section><!-- .fifty-text-media -->

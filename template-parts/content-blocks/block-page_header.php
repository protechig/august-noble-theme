<?php
/**
 * The template used for displaying a CTA block.
 *
 * @package august noble
 */

// Set up fields.
$title           = get_sub_field( 'header_title' );
$text            = get_sub_field( 'summary' );
$animation_class = august_noble_get_animation_class();

// Start a <container> with possible block options.
august_noble_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container page-header', // Container class.
	)
);
?>
<div class=" center-text grid-x<?php echo esc_attr( $animation_class ); ?>">
<?php if ( $title ) : ?>
<h2><?php the_sub_field( 'header_title' ); ?></h2>
<?php endif; ?>

<?php if ( $text ) : ?>
<p><?php echo esc_html( $text ); ?></p>
<?php endif; ?>
</div><!-- .grid-x -->
</section><!-- .cta-block -->

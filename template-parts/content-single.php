<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package august noble
 */

?>

<article <?php post_class(); ?>>


<header class="heading">
<?php
the_title( '<h1 class="entry-title center-text">', '</h1>' );
?>
</header><!-- .entry-header -->

	<div class="thumbnail">
		<?php
		if ( has_post_thumbnail() ) {

			the_post_thumbnail( 'full-width' );
			}
		 ?>
	</div>

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'augustnoble' ), array(
					'span' => array(
						'class' => array(),
					),
				) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'augustnoble' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php august_noble_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

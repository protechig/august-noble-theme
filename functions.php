<?php
/**
 * August noble functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package august noble
 */

if ( ! function_exists( 'august_noble_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function august_noble_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on august noble, use a find and replace
		 * to change 'augustnoble' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'augustnoble', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'blog-thumb', 360, 228, true );
		add_image_size( 'full-width', 943, 525, true );
		add_image_size( 'august', 400, 567, false );
		add_image_size( 'logo', 247, 100, false );
		add_image_size( 'client', 200, 200, false );

		// Register navigation menus.
		register_nav_menus(
			 array(
				 'primary' => esc_html__( 'Primary Menu', 'augustnoble' ),
				 'mobile'  => esc_html__( 'Mobile Menu', 'augustnoble' ),
				 'footer'  => esc_html__( 'Footer Menu', 'augustnoble' ),
			 )
			);

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			 'html5', array(
				 'search-form',
				 'comment-form',
				 'comment-list',
				 'gallery',
				 'caption',
			 )
			);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background', apply_filters(
			 'august_noble_custom_background_args', array(
				 'default-color' => '#FFF',
				 'default-image' => '',
			 )
			)
			);

		// Custom logo support.
		add_theme_support(
			 'custom-logo', array(
				 'height'      => 250,
				 'width'       => 500,
				 'flex-height' => true,
				 'flex-width'  => true,
				 'header-text' => array( 'site-title', 'site-description' ),
			 )
			);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
// August_noble_setup.
add_action( 'after_setup_theme', 'august_noble_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function august_noble_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'august_noble_content_width', 640 );
}
add_action( 'after_setup_theme', 'august_noble_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function august_noble_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1' => esc_html__( 'Sidebar 1', 'augustnoble' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar(
			 array(
				 'name'          => $sidebar_name,
				 'id'            => $sidebar_id,
				 'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'augustnoble' ), $sidebar_name ),
				 'before_widget' => '<aside class="widget %2$s">',
				 'after_widget'  => '</aside>',
				 'before_title'  => '<h2 class="widget-title">',
				 'after_title'   => '</h2>',
			 )
			);
	}

}
add_action( 'widgets_init', 'august_noble_widgets_init' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom ACF search functionality.
 */
require get_template_directory() . '/inc/acf-search.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

/**
 * Testimonial cpt.
 */
function client_testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'august' ),
		'singular_name'         => _x( 'testimonial', 'Post Type Singular Name', 'august' ),
		'menu_name'             => __( 'Testimonials', 'august' ),
		'name_admin_bar'        => __( 'Testimonials', 'august' ),
		'archives'              => __( 'Item Archives', 'august' ),
		'attributes'            => __( 'Item Attributes', 'august' ),
		'parent_item_colon'     => __( 'Parent Item:', 'august' ),
		'all_items'             => __( 'All Items', 'august' ),
		'add_new_item'          => __( 'Add New Item', 'august' ),
		'add_new'               => __( 'Add New', 'august' ),
		'new_item'              => __( 'New Item', 'august' ),
		'edit_item'             => __( 'Edit Item', 'august' ),
		'update_item'           => __( 'Update Item', 'august' ),
		'view_item'             => __( 'View Item', 'august' ),
		'view_items'            => __( 'View Items', 'august' ),
		'search_items'          => __( 'Search Item', 'august' ),
		'not_found'             => __( 'Not found', 'august' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'august' ),
		'featured_image'        => __( 'Featured Image', 'august' ),
		'set_featured_image'    => __( 'Set featured image', 'august' ),
		'remove_featured_image' => __( 'Remove featured image', 'august' ),
		'use_featured_image'    => __( 'Use as featured image', 'august' ),
		'insert_into_item'      => __( 'Insert into item', 'august' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'august' ),
		'items_list'            => __( 'Items list', 'august' ),
		'items_list_navigation' => __( 'Items list navigation', 'august' ),
		'filter_items_list'     => __( 'Filter items list', 'august' ),
	);
	$args   = array(
		'label'               => __( 'testimonial', 'august' ),
		'description'         => __( 'What they say about me', 'august' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'testimonials', $args );

}
add_action( 'init', 'client_testimonial', 0 );
/**
 * Adding color placeholders.
 */
function august_noble_colorpicker_admin_footer() {
	?>
	<script>
	acf.add_filter('color_picker_args', function( args, $field ){
		// do something to args
		args.palettes = ['#F8FCFF','#FFF',]
		// return
		return args;			
	});
	</script>
	<?php
}
add_action( 'acf/input/admin_footer', 'august_noble_colorpicker_admin_footer' );

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * Adding color placeholders.
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
		);
}

/**
 * Adding custom except.
 *
 * @param Limit $limit add limit.
 * @return except limit the excerpt.
 */
function august_custom_exerpt( $limit ) {
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	if ( count( $excerpt ) >= $limit ) {
	  array_pop( $excerpt );
	  $excerpt = implode( ' ', $excerpt ) . '.';
	} else {
	  $excerpt = implode( ' ', $excerpt );
	}
	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
	return $excerpt;
  }

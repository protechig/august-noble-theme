<?php
/**
 * Customizer panels.
 *
 * @package august noble
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function august_noble_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'augustnoble' ),
			'description'    => esc_html__( 'Other theme options.', 'augustnoble' ),
		)
	);
}
add_action( 'customize_register', 'august_noble_customize_panels' );

<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package august noble
 */

?>

</div><!-- #content -->

	<footer class="site-footer">
		<div class="grid-x">
			<div class="site-info">
				
				<div class="site-copyright">
				 <?php august_noble_display_copyright_text(); ?>
					<nav id="site-navigation" class="footer-navigation" aria-label="<?php esc_html_e( 'Footer Menu', 'augustnoble' ); ?>">
						<?php
						wp_nav_menu(
								array(
									'fallback_cb'     => false,
									'theme_location'  => 'footer',
									'menu_id'         => 'footer-menu',
									'menu_class'      => 'menu dropdown',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								)
							);
						?>
					</nav>
				</div>
				<div class="soscial-icons">
						<?php august_noble_display_social_network_links(); ?>
				</div>
				
			</div><!-- .site-info -->
		</div>
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<?php august_noble_display_mobile_menu(); ?>

</body>
</html>

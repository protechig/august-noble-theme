<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package august noble
 */

get_header(); ?>

	<div class="content-area">
	<main id="main" class="site-main">
	<div class="grid-x">
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
	</div>
		</main><!-- #main -->
	</div><!-- .primary -->

	<section class="grid-container  newsletter-signup" style="background-color:<?php the_field( 'background_color', 'option' ); ?>">
		<div class="grid-x">
		<h2 class="subscribe-header"><?php the_field( 'header_text', 'option' ); ?></h2>
		<h3><?php the_field( 'subtitle_text', 'option' ); ?></h3>
		<?php
			$form = get_field( 'subscribe_form', 'option' );
			gravity_form( $form, false, true, false, '', true, 1 );
		?>
		</div><!-- .grid-x -->
	</section><!-- .cta-block -->\

<?php get_footer(); ?>
